// pages/myBuy/myBuy.js
Page({

   /**
    * 页面的初始数据
    先获取到自己的定位
    再拿到茶喵喵的定位
    比较经纬度
    调用接口，得到对应的经纬度
    */
   data: {
      mendianList: [],
      myAddress: {
         name: '南京市新街口明瓦廊146号',
         distance: 0
      },
      activeType: 0,
      firstindex: 0,
      animationData: {},
      goodsList: [],
      menuList: [],
      judgeHidden: true, //判断是否显示选规格窗口
      // judgeHidden: false,
      guige: ["M杯", "L杯（￥3）"],
      wendu: ["正常", "少冰", "去冰", "热品"],
      tiandu: ["多糖", "七分糖", "半塘", "不要糖"],
      _guige: 0,
      _wendu: 0,
      _tiandu: 0,
      myGuige: 'M杯',
      myWendu: '正常',
      myTiandu: '多糖',
      clickItem: [] //被点击的这条数据
   },
   //加入购物车
   intocar: function(e) {
      let that = this
      wx.showToast({
         title: '加入成功',
         icon: 'success',
         duration: 1000,
         complete: function() {
            setTimeout(function() {
               that.setData({
                     judgeHidden: true
                  }), wx.switchTab({
                     url: '../myCart/myCart',
                  }),
                  wx.setStorageSync('clickItem', that.data.clickItem); //将点击的商品存入本地缓存
               //将商品规格存入本地缓存
            }, 1000)

         }
      })
   },
   // 选规格
   chooseguige: function(e) {
      let that = this
      that.data.guige.forEach((item, index) => {
         if (index == e.currentTarget.dataset.index) {
            that.setData({
               _guige: index,
               myGuige: that.data.guige[e.currentTarget.dataset.index]
            })
         }
      })
   },
   //选甜度
   choosetiandu: function(e) {
      let that = this
      that.data.tiandu.forEach((item, index) => {
         if (index == e.currentTarget.dataset.index) {
            that.setData({
               _tiandu: index,
               myTiandu: that.data.tiandu[e.currentTarget.dataset.index]
            })
         }
      })
   },
   //选温度
   choosewendu: function(e) {
      let that = this
      that.data.wendu.forEach((item, index) => {
         if (index == e.currentTarget.dataset.index) {
            that.setData({
               _wendu: index,
               myWendu: that.data.wendu[e.currentTarget.dataset.index]
            })
         }
      })
   },
   // 关闭按钮
   close(e) {
      let that = this
      that.setData({
         judgeHidden: true
      })
      console.log(e)
   },
   //点击选规格触发选项
   showchoose(e) {
      let that = this
      that.setData({
         judgeHidden: false
      })
      console.log(e)
      console.log("*******************块块块*************************")
      console.log(e.currentTarget.dataset.id1)
      console.log("*******************个个个*************************")
      console.log(e.currentTarget.dataset.id2)
      console.log("*******************拿到被点击的数据*************************")
      console.log(that.data.goodsList[e.currentTarget.dataset.id1].goods[e.currentTarget.dataset.id2])
      var clickItem = that.data.goodsList[e.currentTarget.dataset.id1].goods[e.currentTarget.dataset.id2]
      console.log(clickItem)
      that.setData({
         clickItem: clickItem
      })
      console.log(that.data.clickItem)
   },
   //滚动改变左侧样式
   scroll(e) {
      let that = this
      // firstindex = this.Page.data.firstindex
      console.log(e.detail.scrollTop)
      let scrollTop = e.detail.scrollTop
      // var index = 0
      for (var i = 0; i < 7; i++) {
         if ((i + 1) * 534.2 >= scrollTop) {
            // index = i
            that.setData({
               // activeType: i,
               firstindex: i,
            })
            // console.log(this.data.activeType)
            console.log(this.data.firstindex)
            break;
         }
      }


      // if (firstindex != firstindex.data.index) {
      //    that.setData({
      //       firstindex: index
      //    })
      // }
   },

   //点击改变样式的方法
   changeType(e) {
      // let that = this
      console.log(e)
      console.log(e.currentTarget.dataset.type)
      this.setData({
         activeType: e.currentTarget.dataset.type,
         firstindex: e.currentTarget.dataset.type
      })
      // console.log(this.data.activeType)
      console.log(this.data.firstindex)
   },
   ///自提 外卖
   changewuliutype() {
      this.setData({
         isziti: !this.data.isziti
      })
   },
   //选地址的方法
   changeLocation() {
      let that = this
      // wx.chooseLocation({
      //    success: function(res) {
      //       console.log(res)
      //       that.setData({
      //          myAddress: res.address
      //       })
      //    },
      // })

   },


   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function(options) {
      let that = this
      //获取menuList
      wx.request({
         url: 'https://www.easy-mock.com/mock/5d257e0d77744b2808b8840d/study/chaMiaoMiaoMenuList',
         success: function(res) {
            console.log(res)
            that.setData({
               menuList: res.data
            })
            // //选择器查询
            // let q = wx.createSelectorQuery()
            // //获取页面上的节点信息
            // q.selectAll(".section").boundingClientRect(function(res) {
            //    console.log(res)
            // })
            // q.select(".container").boundingClientRect(function(res) {
            //    console.log(res)
            // })
            //执行
            // q.exec()
         }
      })
      //获取goodsList
      wx.request({
         url: 'https://www.easy-mock.com/mock/5d257e0d77744b2808b8840d/study/chaMiaoMiaoGoodsList',
         success: function(res) {
            console.log(res)
            that.setData({
               goodsList: res.data
            })
         }
      })


      //定位
      wx.getLocation({
         type: "gcj02",
         success: function(res) {
            //获取门店的地址
            wx.request({
               url: 'https://apis.map.qq.com/ws/geocoder/v1/',
               data: {
                  // address: "南京市新街口明瓦廊146号",
                  address: that.data.myAddress,
                  key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ"
               },
               success: function(resOne) {
                  console.log(resOne)
                  console.log(resOne.data.result.location)
                  // 获取我自己的位置
                  wx.request({
                     url: 'https://apis.map.qq.com/ws/distance/v1/?parameters',
                     data: {
                        key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ",
                        from: `${res.latitude},${res.longitude}`,
                        to: resOne.data.result.location.lat + "," + resOne.data.result.location.lng,
                        mode: "driving"
                     },
                     success: function(resTwo) {
                        console.log(resTwo.data.result.elements[0].distance) //距离
                        that.data.myAddress.distance = resTwo.data.result.elements[0].distance
                        that.setData({
                           myAddress: that.data.myAddress
                        })
                     }
                  })
               }
            })

         }
      })


      ////////////////////////////////////////////////////////////////////////////////
      // var mendian = [
      //    // "南京市新街口明瓦廊146号",
      //    // "南京市建邺区白龙江东街16号"
      //    {
      //       text: "南京市新街口明瓦廊146号"
      //    },
      //    {
      //       text: "南京市建邺区白龙江东街16号"
      //    }
      // ]


      // var pro = new Promise((resolve, reject) => {
      //    var count = 0
      //    mendian.forEach(function(r) {
      //       wx.request({
      //          url: 'https://apis.map.qq.com/ws/geocoder/v1/',
      //          data: {
      //             address: r.text,
      //             key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ"
      //          },
      //          success: function(res) {
      //             count++
      //             if (count === 2) resolve()
      //             console.log(r)
      //             console.log(res)
      //             console.log(res.data)
      //             console.log(res.data.result.location)
      //             r.location = res.data.result.location
      //          }
      //       })
      //    })
      // })

      // pro.then(function(res) {
      //    //  console.log(res)
      //    // console.log(mendian)
      //    // mendian.forEach((r) => {
      //    //    console.log(r.location)
      //    // })
      //    var locations = mendian.map((r) => {
      //       return r.location.lat + "," + r.location.lng
      //    })
      //    //字符串数组  输出两个门店坐标数组
      //    console.log(locations)
      //    console.log(locations.join(";")) //拼接


      //    //定位
      //    wx.getLocation({
      //       type: "gcj02",
      //       success: (res) => {
      //          //获取我自己的位置
      //          wx.request({
      //             url: 'https://apis.map.qq.com/ws/geocoder/v1/',
      //             data: {
      //                location: `${res.latitude },${res.longitude }`,
      //                key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ"
      //             },
      //             success: (res) => {
      //                console.log("我自己的位置")
      //                console.log(res)
      //                console.log(res.data)
      //                console.log(res.data.result)
      //                console.log(res.data.result.address)
      //                that.setData({
      //                   myAddress: res.data.result.address
      //                })
      //             }
      //          })

      //          console.log(res.latitude, res.longitude)
      //          //获取门店的位置
      //          wx.request({
      //             url: 'https://apis.map.qq.com/ws/distance/v1/?parameters',
      //             data: {
      //                key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ",
      //                from: `${res.latitude},${res.longitude}`,
      //                to: locations.join(";"),
      //                mode: "driving"
      //             },
      //             success: function(res) {
      //                that.setData({
      //                   mendianList: mendian.map((r, i) => {
      //                      return {
      //                         address: r.text,
      //                         distance: res.data.result.elements[i].distance
      //                      }
      //                   })
      //                })
      //                console.log(mendian)
      //             }
      //          })
      //       }
      //    })
      // })
      //////////////////////////////////////////////////////////////////////////////////





      //***************************************************************************** */
      // wx.request({
      //    url: 'https://apis.map.qq.com/ws/geocoder/v1/',
      //    data: {
      //       address: `南京市新街口明瓦廊146号`,
      //       // address: `茶喵喵新街口店`,
      //       // address: `茶喵喵`,
      //       key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ"
      //    },
      //    success: function(res) {
      //       console.log(res)
      //       console.log(res.data)
      //       console.log(res.data.result)
      //       console.log(res.data.result.location)
      //       console.log("茶喵喵坐标")
      //       console.log(res.data.result.location.lng, res.data.result.location.lat)
      //    }
      // })


      // //定位
      // wx.getLocation({
      //    type: "gcj02",
      //    success: function(res) {
      //       wx.request({
      //          url: 'https://apis.map.qq.com/ws/geocoder/v1/',
      //          // https://apis.map.qq.com/ws/distance/v1/
      //          data: {
      //             address: `南京市新街口明瓦廊146号`,
      //             // address: `茶喵喵新街口店`,
      //             key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ"
      //          },
      //          success: function(res1) {
      //             // console.log("茶喵喵坐标")
      //             // console.log(res.data.result.location.lng, res.data.result.location.lat)
      //             wx.request({
      //                url: 'https://apis.map.qq.com/ws/distance/v1/',
      //                data: {
      //                   mode: "walking",
      //                   from: `${res.latitude},${res.longitude}`,
      //                   to: `${res1.data.result.location.lat},${res1.data.result.location.lng}`, //维精
      //                   key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ"
      //                },
      //                success: function(res2) {
      //                   console.log("距离")
      //                   console.log(res2.data)
      //                   console.log(res2.data.result)
      //                   console.log(res2.data.result.elements[0])
      //                   console.log(res2.data.result.elements[0].distance)
      //                }
      //             })
      //          }
      //       })

      // console.log(res)
      // console.log(res.longitude, res.latitude) //经纬度 
      // wx.request({
      //    url: 'https://apis.map.qq.com/ws/geocoder/v1/',
      //    data: {
      //       // location:"39.984154, 116.307490",
      //       location: `${res.latitude },${res.longitude}`,
      //       key: "J3LBZ-ERWCU-RX4VX-2NUQQ-NOBY5-2VFGJ"
      //    },
      //    success:function(res){
      //       console.log(res)
      //       console.log(res.data) 
      //       console.log(res.data.result)
      //       console.log(res.data.result.address)
      //    }
      // })
      // },
      // })
      //***************************************************************************** */


   },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function() {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   // onShow: function() {
   //    var animation = wx.createAnimation({
   //       duration: 1000,
   //       timingFunction: 'ease',
   //    })

   //    this.animation = animation

   //    // animation.scale(2, 2).rotate(45).step()

   //    this.setData({
   //       animationData: animation.export()
   //    })

   //    setTimeout(function() {
   //       animation.translate(30).step()
   //       this.setData({
   //          animationData: animation.export()
   //       })
   //    }.bind(this), 1000)
   // },
   // rotateAndScale: function() {
   //    // 旋转同时放大
   //    this.animation.rotate(45).scale(2, 2).step()
   //    this.setData({
   //       animationData: this.animation.export()
   //    })
   // },
   // rotateThenScale: function() {
   //    // 先旋转后放大
   //    this.animation.rotate(45).step()
   //    this.animation.scale(2, 2).step()
   //    this.setData({
   //       animationData: this.animation.export()
   //    })
   // },
   // rotateAndScaleThenTranslate: function() {
   //    // 先旋转同时放大，然后平移
   //    this.animation.rotate(45).scale(2, 2).step()
   //    this.animation.translate(100, 100).step({
   //       duration: 1000
   //    })
   //    this.setData({
   //       animationData: this.animation.export()
   //    })
   // },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function() {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function() {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function() {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function() {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function() {

   }
})