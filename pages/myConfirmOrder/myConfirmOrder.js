// pages/myConfirmOrder/myConfirmOrder.js
Page({

   /**
    * 页面的初始数据
    */
   data: {
      allClickItem: [],
      hidden: true,
      priceCount: 0
   },
 
   //确认支付
   payNow: function() {
      wx.showModal({
         title: '提示',
         content: '确认要支付吗？',
         success: function(res) {
            if (res.confirm) {
               console.log('用户点击确定')
               wx.showToast({
                  title: '支付中...',
                  icon: "loading",
                  duration: 1000,
                  success: function() {
                     setTimeout(function() {
                        wx.showToast({
                           title: '支付成功',
                           icon: 'success',
                           duration: 1000,
                           success: function() {
                              setTimeout(function() {
                                 wx.switchTab({
                                    url: '../myOrder/myOrder',
                                 })
                              }, 1500)
                           }
                        })
                     }, 1500)
                  }
               })
            } else {
               console.log('用户点击取消')

               wx.showToast({
                  title: '取消中...',
                  icon: "loading",
                  duration: 1500,
               })
            }
         }
      })
   },
   ///自提 外卖
   changewuliutype() {
      this.setData({
         isziti: !this.data.isziti
      })
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function(options) {

   },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function() {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function() {
      let that = this
      wx.getStorage({
         key: 'allClickItem',

         success: function(res) {
            console.log("传过来的结算商品")
            console.log(res.data)
            that.setData({
               hidden: false
            })
            that.setData({
               allClickItem: res.data
            })
            var priceCount = 0 ////定义总支付金额  承接
            //总支付金额
            res.data.forEach(function(item, index) {
               console.log(item)
               priceCount += item.num * item.price
            })
            that.setData({
               priceCount: priceCount
            })
         },
         fail: function(res) {
            that.setData({
               hidden: true
            })
            wx.showToast({
               title: '没有需要支付的订单',
               icon: "loading",
               success: function() {
                  setTimeout(function() {
                     wx.switchTab({
                        url: '../myBuy/myBuy',
                     })
                  }, 2000)
               }
            })
         }


         // success(res) {
         //    console.log("传过来的结算商品")
         //    console.log(res.data)
         //    that.setData({
         //       allClickItem: res.data
         //    })

         //    //根据内容判断是否显示
         //    if (res.data == '') {
         //       that.setData({
         //          hidden: true
         //       })
         //       wx.showToast({
         //          title: '没有需要支付的订单',
         //          icon: "loading",
         //          success: function() {
         //             setTimeout(function() {
         //                wx.switchTab({
         //                   url: '../myBuy/myBuy',
         //                })
         //             }, 2000)
         //          }
         //       })
         //    } else if (res.data != '') { //如果res里面有数据的话
         //       that.setData({
         //          hidden: false
         //       })

         //       var priceCount = 0 ////定义总支付金额  承接
         //       //总支付金额
         //       res.data.forEach(function(item, index) {
         //          console.log(item)
         //          priceCount += item.num * item.price
         //       })
         //       that.setData({
         //          priceCount: priceCount
         //       })
         //    }
         // }



      })
   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function() {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function() {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function() {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function() {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function() {

   }
})