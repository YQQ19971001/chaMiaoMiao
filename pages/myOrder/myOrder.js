// pages/myOrder/myOrder.js
Page({

   /**
    * 页面的初始数据
    */
   data: {
      allClickItem: [], //从本地获取的单条订单
      hidden: true,
      // 获取过来的  保存所有的订单数据的缓存
      allPageOrder: []
   },


   //删除本条订单
   deleteOrder: function(e) {
      console.log(e)
      let that = this
      console.log("输出所有allPageOrder ")
      console.log(that.data.allPageOrder)



      wx.showModal({
         title: '提示',
         content: '确定要删除本条历史订单吗？',
         success: function(res) {
            if (res.cancel) {
               console.log("点击了取消")
            } else if (res.confirm) {
               console.log("点击了确定")
               //获取被点击的item的下标
               var index = e.currentTarget.dataset.id; //获取当前订单下标
               console.log("下标" + index)
               console.log("该下标的数据！")
               console.log(that.data.allPageOrder[index])
               var allPageOrder = that.data.allPageOrder
               console.log("页面page上的allPageOrder")
               console.log(allPageOrder)

               var getLocalOrder = wx.getStorageSync('allPageOrder')
               console.log("本地的allPageOrder")
               console.log(getLocalOrder)

               allPageOrder = allPageOrder.splice(index, 1) //去除页面上的点击删除的数据
               console.log("去除页面上的点击删除的数据后的 allPageOrder")
               console.log(allPageOrder)
               wx.setStorageSync("allPageOrder", that.data.allPageOrder)

               that.setData({
                  allPageOrder: wx.getStorageSync('allPageOrder')
               })
            }
         }
      })
   },



   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function(options) {

   },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function() {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function() {
      let that = this
      wx.getStorage({
         key: 'allClickItem',
         success(res) {
            console.log(res)
            console.log(res.data)

            //把单条订单放入订单缓存
            var allPageOrder = that.data.allPageOrder.concat(res.data)
            console.log(allPageOrder)
            wx.setStorage({
               key: 'allPageOrder',
               data: allPageOrder, //本地保存的是所有订单
            })
            // //原
            // that.setData({
            //    allClickItem: res.data,
            //    hidden: false
            // })
         },
         fail: function(res) {
            console.log(res)
            wx.showToast({
               title: '购物车暂无更新...',
               icon: 'loading',
               duration: 1000
            })
            that.setData({
               hidden: true
            })
         }
      })

      // var allPageOrder = []
      wx.getStorage({
         key: 'allPageOrder',
         success: function(res) {
            // allPageOrder = res.data 
            that.setData({
               allPageOrder: res.data,
               hidden: false
            })
            wx.removeStorage({
               key: 'allClickItem',
               success: function(res) {
                  console.log("已移除allClickItem单条订单，总订单已经存到allPageOrder")
               },
            })
         },
      })

      //   that.setData({
      //      allPageOrder: allPageOrder
      //   })
   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function() {

   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function() {

   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function() {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function() {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function() {

   }
})