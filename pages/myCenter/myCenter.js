// pages/myCenter/myCenter.js
Page({
  kefu:function(){
    wx.makePhoneCall({
      phoneNumber: '18951615946',
    })
  },
  dingdan:function(){
    wx.switchTab({
      url: '../../pages/myOrder/myOrder',
    })
  },
  goCup:function(){
    wx.navigateTo({
      url: '../../pages/myCup/myCup',
    })
  },
  goVip: function () {
    wx.navigateTo({
      url: '../../pages/VIP/vip',
    })
  },
   /**
    * 页面的初始数据
    */
   data: {
     canIUse: wx.canIUse('button.open-type.getUserInfo'),
     hasUser: false
   },
   /**
    * 生命周期函数--监听页面加载
    */
   onLoad: function (options) {
     let that = this
     wx.getSetting({
       success(res) {
         if (res.authSetting['scope.userInfo']) {
           // 已经授权，可以直接调用 getUserInfo 获取头像昵称
           wx.getUserInfo({
             success: function (res) {
               console.log(res.userInfo) 
             }
           })
          // wx.switchTab({
          //   url: '',
          // })
           that.setData({
             hasUser:true
           })
         }
       }
     })
   },
  bindGetUserInfo(e) {
    console.log(e.detail)
    console.log(e.detail.userInfo)
  },

   /**
    * 生命周期函数--监听页面初次渲染完成
    */
   onReady: function () {

   },

   /**
    * 生命周期函数--监听页面显示
    */
   onShow: function () {

   },

   /**
    * 生命周期函数--监听页面隐藏
    */
   onHide: function () {

   },
   dizhi:function(){
     wx.switchTab({
       url: '/pages/addressList/addressList'
     })
   },

   /**
    * 生命周期函数--监听页面卸载
    */
   onUnload: function () {
   },

   /**
    * 页面相关事件处理函数--监听用户下拉动作
    */
   onPullDownRefresh: function () {

   },

   /**
    * 页面上拉触底事件的处理函数
    */
   onReachBottom: function () {

   },

   /**
    * 用户点击右上角分享
    */
   onShareAppMessage: function () {

   }
})